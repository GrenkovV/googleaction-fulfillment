package com.comp.SereviceGoogleHome.models;

import java.util.UUID;

public class AccessTokenResponse {
    private String access_token;
    private String token_type = "bearer";
    private Long expires_in = 604800L; // 7 days expire
    private String refresh_token;

    public AccessTokenResponse() {
        this.access_token = UUID.randomUUID().toString();
        this.refresh_token = UUID.randomUUID().toString();
    }

    public AccessTokenResponse(String access_token, String token_type, Long expires_in, String refresh_token) {
        this.access_token = access_token;
        this.token_type = token_type;
        this.expires_in = expires_in;
        this.refresh_token = refresh_token;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Long expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public void clear() {
        this.access_token = "";
        this.refresh_token = "";
        this.token_type = "";
        this.expires_in = 0L;
    }

    @Override
    public String toString() {
        return "AccessTokenResponse{" +
                "access_token='" + access_token + '\'' +
                ", token_type='" + token_type + '\'' +
                ", expires_in=" + expires_in +
                ", refresh_token='" + refresh_token + '\'' +
                '}';
    }
}
