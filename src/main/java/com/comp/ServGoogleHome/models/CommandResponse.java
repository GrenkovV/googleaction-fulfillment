package com.comp.SereviceGoogleHome.models;

public class CommandResponse {

    private boolean isSuccess;
    private Object result;

    public CommandResponse(Boolean isSuccess) {
        if (isSuccess != null)
            this.isSuccess = isSuccess;
        else this.isSuccess = false;
        this.result = null;
    }

    public CommandResponse(Boolean isSuccess, Object result) {
        if (isSuccess != null)
            this.isSuccess = isSuccess;
        else this.isSuccess = false;
        this.result = result;
    }

    public static CommandResponse failed() {
        return new CommandResponse(false);
    }

    public static CommandResponse failed(Object data) {
        return new CommandResponse(false, data);
    }

    public static CommandResponse success() {
        return new CommandResponse(true);
    }

    public static CommandResponse success(Object data) {
        return new CommandResponse(true, data);
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
