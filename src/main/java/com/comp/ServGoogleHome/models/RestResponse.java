package com.comp.SereviceGoogleHome.models;

public class RestResponse {
    public String message = "";
    public String status = "";
    public Object data = null;

    public static RestResponse asSuccess(String message) {
        return asSuccess(message, null);
    }
    public static RestResponse asSuccess(String message, Object data) {
        RestResponse resp = new RestResponse();
        resp.status = "OK";
        resp.message = message;
        resp.data = data;

        return resp;
    }

    public static RestResponse asError(String message) {
        return asError(message, null);
    }
    public static RestResponse asError(String message, Object data) {
        RestResponse resp = new RestResponse();
        resp.status = "NOK";
        resp.message = message;
        resp.data = data;

        return resp;
    }
}