package com.comp.SereviceGoogleHome.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.List;

@DynamoDBTable(tableName="SereviceAuth")
public class UserInfo {
    private String id;
    private String username;
    private String email;
    private List<DeviceInfo> devices;
    private String accessToken;
    private String refreshToken;
    private Long ttl;

    public UserInfo() {
    }

    public UserInfo(String id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
    }

    @DynamoDBAttribute(attributeName = "uid")
    public String getId() {
        return id;
    }

    public UserInfo setId(String id) {
        this.id = id;
        return this;
    }

    @DynamoDBAttribute(attributeName = "userName")
    public String getUsername() {
        return username;
    }

    public UserInfo setUsername(String username) {
        this.username = username;
        return this;
    }

    @DynamoDBAttribute(attributeName = "userEmail")
    public String getEmail() {
        return email;
    }

    public UserInfo setEmail(String email) {
        this.email = email;
        return this;
    }

    public List<DeviceInfo> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceInfo> devices) {
        this.devices = devices;
    }

    @DynamoDBHashKey(attributeName = "AccessKey")
    public String getAccessToken() {
        return accessToken;
    }

    public UserInfo setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    @DynamoDBAttribute(attributeName = "RefreshToken")
    public String getRefreshToken() {
        return refreshToken;
    }

    public UserInfo setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    @DynamoDBAttribute(attributeName = "ttl")
    public Long getTtl() {
        if(ttl == null)
            ttl = System.currentTimeMillis() / 1000 + 30 * 24 * 60 * 60; // 30 days TTL
        return ttl;
    }

    public UserInfo setTtl(Long ttl) {
        this.ttl = ttl;
        return this;
    }

    public UserInfo makeComplementary() {
        String accessToken = this.getAccessToken();
        this.setAccessToken(this.getRefreshToken());
        this.setRefreshToken(accessToken);

        return this;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                '}';
    }
}
