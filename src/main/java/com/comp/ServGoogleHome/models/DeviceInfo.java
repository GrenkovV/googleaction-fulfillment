package com.comp.SereviceGoogleHome.models;

import com.comp.SereviceGoogleHome.handlers.DeviceTypes;

import java.util.HashMap;
import java.util.Map;

public class DeviceInfo {

    private String devId;
    private String name;
    private String customName;
    private String uid;
    private Map<String, Object> dps = new HashMap<>();
    private Boolean isActive;
    private Boolean isOnline;
    private String timeZoneStr;

    public DeviceInfo() {
    }

    public DeviceInfo(String customName, String devId, String name, String uid, Boolean isActive, Boolean isOnline) {
        this.customName = customName;
        this.devId = devId;
        this.name = name;
        this.uid = uid;
        this.isActive = isActive;
        this.isOnline = isOnline;
    }

    public void syncDps(Map<String, Object> dps) {
        for (String key : dps.keySet()) {
            Object value = dps.get(key);
            this.dps.put(key, value);
        }
    }

    public String getDevId() {
        return devId;
    }

    public DeviceInfo setDevId(String devId) {
        this.devId = devId;
        return this;
    }

    public String getName() {
        return name;
    }

    public DeviceInfo setName(String name) {
        this.name = name;
        return this;
    }

    public String getCustomName() {
        return customName;
    }

    public DeviceInfo setCustomName(String customName) {
        this.customName = customName;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public DeviceInfo setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Map<String, Object> getDps() {
        return dps;
    }

    public DeviceInfo setDps(Map<String, Object> dps) {
        this.dps = dps;
        return this;
    }

    public Boolean getActive() {
        return isActive;
    }

    public DeviceInfo setActive(Boolean active) {
        isActive = active;
        return this;
    }

    public Boolean getOnline() {
        return isOnline;
    }

    public DeviceInfo setOnline(Boolean online) {
        isOnline = online;
        return this;
    }

    public String getTimeZoneStr() {
        return timeZoneStr;
    }

    public DeviceInfo setTimeZoneStr(String timeZoneStr) {
        if (timeZoneStr != null)
            timeZoneStr = timeZoneStr.replaceAll("\\s", "");
        this.timeZoneStr = timeZoneStr;
        return this;
    }

    public DeviceTypes detectType() {
        switch (this.getDps().keySet().size()) {
            case 1:
            case 2:
                return DeviceTypes.OUTLET;
            case 6:
                return DeviceTypes.POWER_STRIP;
            case 9:
            case 10:
                return DeviceTypes.LIGHT_BULB;
            default:
                return DeviceTypes.UNKNOWN;
        }
    }
}
