package com.comp.SereviceGoogleHome.handlers;

import com.comp.SereviceGoogleHome.commands.Command;
import com.comp.SereviceGoogleHome.commands.CommandBuilder;
import com.comp.SereviceGoogleHome.commands.CommandBuilderFactory;
import com.comp.SereviceGoogleHome.controllers.LoginController;
import com.comp.SereviceGoogleHome.models.CommandResponse;
import com.comp.SereviceGoogleHome.models.DeviceInfo;
import com.comp.SereviceGoogleHome.models.Fulfillment;
import com.comp.SereviceGoogleHome.services.SereviceCloudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LightColorHandler extends SmartHomeHandler {

    private final Logger logger = LoggerFactory.getLogger(LightColorHandler.class);

    @Override
    public Fulfillment process(String accessToken, Map<String, String> params, SereviceCloudService SereviceCloudService, String userId) throws RuntimeException {
        List<DeviceInfo> devices = filterDevices(SereviceCloudService.getDeviceList(userId, false), params, DeviceTypes.LIGHT_BULB);
        if (devices.isEmpty()) {
            return new Fulfillment("Sorry, but I can't find such device. What should I do next?");
        }
        devices.forEach(deviceInfo -> logger.info("Device[" + deviceInfo.getDevId() + "]: " + deviceInfo.getName() + " | " + deviceInfo.getCustomName()));

        List<CommandResponse> executionResults = devices.stream().map(device -> {
            CommandBuilder commandBuilder = CommandBuilderFactory.getCommandBuilder(device);
            Command command = commandBuilder.buildCommand(device, params);
            if (command != null) {
                CommandResponse execRes = command.execute(SereviceCloudService);
                if(execRes == null)
                    return CommandResponse.failed();
                else
                    return execRes;
            }
            else return CommandResponse.failed();
        }).collect(Collectors.toList());

        long successCount = executionResults.stream().filter(item -> item.isSuccess()).count();
        long errorsCount = executionResults.stream().filter(item -> !item.isSuccess()).count();

        String targetColor = ParamsConst.getStringParam(params, ParamsConst.PARAM_COLOR_NAME, "");

        /*if (!params.get(ParamsConst.PARAM_IN_TIME).isEmpty() || !params.get(ParamsConst.PARAM_AT_TIME).isEmpty()) {
            if (errorsCount == 0) {
                return new Fulfillment("Lamps color is set!");
            } else if (successCount == 0) {
                return new Fulfillment("Sorry, but I can't set color for specified device. What should I do next?");
            } else {
                return new Fulfillment("I set color to " + successCount + " from " + devices.size() + " lamps!");
            }
            return new Fulfillment("Lamps color is set to " + targetColor + "!");
        } else {*/
            if (errorsCount == 0) {
                return new Fulfillment("Lamps color is set to " + targetColor + "! What should I do next?");
            } else if (successCount == 0) {
                return new Fulfillment("Sorry, but I can't set specified color. What should I do next?");
            } else {
                return new Fulfillment("I set " + targetColor + " color to " + successCount + " from " + devices.size() + " lamps! What should I do next?");
            }
        //}
    }
}
