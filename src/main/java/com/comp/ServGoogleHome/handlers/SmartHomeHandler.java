package com.comp.SereviceGoogleHome.handlers;

import com.comp.SereviceGoogleHome.models.DeviceInfo;
import com.comp.SereviceGoogleHome.models.Fulfillment;
import com.comp.SereviceGoogleHome.services.SereviceCloudService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class SmartHomeHandler {

    public abstract Fulfillment process(String accessToken, Map<String, String> params, SereviceCloudService SereviceCloudService, String userId) throws RuntimeException;

    public List<DeviceInfo> filterDevices(List<DeviceInfo> devices, Map<String, String> params) {
        return filterDevices(devices, params, DeviceTypes.UNKNOWN);
    }

    public List<DeviceInfo> filterDevices(List<DeviceInfo> devices, Map<String, String> params, DeviceTypes deviceType) {

        String room = ParamsConst.getStringParam(params, ParamsConst.PARAM_ROOM, "");
        String device = ParamsConst.getStringParam(params, ParamsConst.PARAM_DEVICE, "");

        List<String> filterParts = new ArrayList<>();
        filterParts.addAll(Arrays.asList(room.toLowerCase().split("\\s")));
        filterParts.addAll(Arrays.asList(device.toLowerCase().split("\\s")));
        filterParts = filterParts.stream().filter(item -> item != null && !item.trim().isEmpty()).collect(Collectors.toList());
        Integer deviceTypeParam = ParamsConst.getIntegerParam(params, ParamsConst.PARAM_DEVICE_TYPE, 0);
        switch (deviceTypeParam) {
            case 1:
                deviceType = DeviceTypes.LIGHT_BULB;
                break;
            case 2:
                deviceType = DeviceTypes.POWER_STRIP;
                break;
            case 3:
                deviceType = DeviceTypes.OUTLET;
                break;
        }

        boolean swWrongDataSuspicion = false;
        List<DeviceInfo> filteredDevices = devices;
        for (String part : filterParts) {
            List<DeviceInfo> devicesBuff = filteredDevices.stream().filter(deviceInfo -> deviceInfo.getCustomName().toLowerCase().contains(part))
                    .collect(Collectors.toList());
            //if (!devicesBuff.isEmpty()) {
                filteredDevices = devicesBuff;
                //swWrongDataSuspicion = true;
            //}
        }

        // If ROOM or DEVICE was set, but no filtration happened, then parameters is wrong
        /*if ((!room.isEmpty() || !device.isEmpty()) &&
                filteredDevices.size() == devices.size() &&
                swWrongDataSuspicion) {
            return new ArrayList<>();
        }*/

        if (deviceType != DeviceTypes.UNKNOWN) {
            DeviceTypes finalDeviceType = deviceType;
            filteredDevices = filteredDevices.stream().filter(deviceInfo -> deviceInfo.detectType() == finalDeviceType).collect(Collectors.toList());
        }

        return filteredDevices;
    }
}
