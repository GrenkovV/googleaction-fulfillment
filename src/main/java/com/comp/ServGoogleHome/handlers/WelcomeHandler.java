package com.comp.SereviceGoogleHome.handlers;

import com.comp.SereviceGoogleHome.models.Fulfillment;
import com.comp.SereviceGoogleHome.services.SereviceCloudService;

import java.util.Map;

public class WelcomeHandler extends SmartHomeHandler {
    @Override
    public Fulfillment process(String accessToken, Map<String, String> params, SereviceCloudService SereviceCloudService, String userId) {
        return new Fulfillment("Welcome to Serevice Smart Home! How can I help?");
    }
}
