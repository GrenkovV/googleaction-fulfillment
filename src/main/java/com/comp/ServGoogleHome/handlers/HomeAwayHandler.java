package com.comp.SereviceGoogleHome.handlers;

import com.comp.SereviceGoogleHome.commands.Command;
import com.comp.SereviceGoogleHome.commands.CommandBuilder;
import com.comp.SereviceGoogleHome.commands.CommandBuilderFactory;
import com.comp.SereviceGoogleHome.controllers.LoginController;
import com.comp.SereviceGoogleHome.models.CommandResponse;
import com.comp.SereviceGoogleHome.models.DeviceInfo;
import com.comp.SereviceGoogleHome.models.Fulfillment;
import com.comp.SereviceGoogleHome.services.SereviceCloudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HomeAwayHandler extends SmartHomeHandler {

    private final Logger logger = LoggerFactory.getLogger(HomeAwayHandler.class);

    @Override
    public Fulfillment process(String accessToken, Map<String, String> params, SereviceCloudService SereviceCloudService, String userId) throws RuntimeException {
        if (ParamsConst.getBooleanParamNullable(params, ParamsConst.PARAM_HOME_AWAY, "1") == null)
            return new Fulfillment("Sorry, but I don't understand... What should I do next?");


        List<DeviceInfo> devices = filterDevices(SereviceCloudService.getDeviceList(userId, false), params);
        if (devices.isEmpty()) {
            return new Fulfillment("Sorry, but I can't find such device. What should I do next?");
        }
        devices.forEach(deviceInfo -> {
            logger.info("Device[" + deviceInfo.getDevId() + "]: " + deviceInfo.getName() + " | " + deviceInfo.getCustomName());
        });

        List<CommandResponse> executionResults = devices.stream().map(device -> {
            CommandBuilder commandBuilder = CommandBuilderFactory.getCommandBuilder(device);
            Command command = commandBuilder.buildCommand(device, params);
            if (command != null) {
                CommandResponse execRes = command.execute(SereviceCloudService);
                if(execRes == null)
                    return CommandResponse.failed();
                else
                    return execRes;
            }
            else return CommandResponse.failed();
        }).collect(Collectors.toList());

        long successCount = executionResults.stream().filter(item -> item.isSuccess()).count();
        long errorsCount = executionResults.stream().filter(item -> !item.isSuccess()).count();

        String targetStateStr = "off";
        if (ParamsConst.getBooleanParam(params, ParamsConst.PARAM_HOME_AWAY, false, "1"))
            targetStateStr = "on";

        if (errorsCount == 0) {
            return new Fulfillment("All devices are turned " + targetStateStr + "! What should I do next?");
        } else if (successCount == 0) {
            return new Fulfillment("Sorry, but I can't turn " + targetStateStr + " devices! What should I do next?");
        } else {
            return new Fulfillment("I turned " + targetStateStr + " " + successCount + " from " + devices.size() + " devices! What should I do next?");
        }
    }
}
