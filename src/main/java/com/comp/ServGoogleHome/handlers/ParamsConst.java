package com.comp.SereviceGoogleHome.handlers;

import java.util.Map;

public class ParamsConst {
    public static final String PARAM_DEVICE = "DEVICE";
    public static final String PARAM_ROOM = "ROOM";
    public static final String PARAM_DEVICE_TYPE = "DEVICE_TYPE";
    public static final String PARAM_POWER_ST_SOCKET = "POWER_ST_SOCKET";
    public static final String PARAM_POWER_ACTION = "POWER_ACTION";
    public static final String PARAM_IN_TIME = "IN_TIME";
    public static final String PARAM_AT_TIME = "AT_TIME";
    public static final String PARAM_CHECK_STATUS = "CHECK_STATUS";
    public static final String PARAM_ON_OFF = "ON_OFF";
    public static final String PARAM_DEGREES = "DEGREES";
    public static final String PARAM_COLOR_NAME = "COLOR_NAME";
    public static final String PARAM_PERCENT = "PERCENT";
    public static final String PARAM_WHITE_LIGHT_MODE = "WHITE_LIGHT_MODE";
    public static final String PARAM_BRIGHTEN_DIM = "BRIGHTEN_DIM";
    public static final String PARAM_SET_BRIGHTNESS = "SET_BRIGHTNESS";
    public static final String PARAM_LIGHT_MOOD = "MOOD";
    public static final String PARAM_HOME_AWAY = "HOME_AWAY";

    public static String getStringParam(Map<String, String> params, String key, String defaultValue) {
        return params.getOrDefault(key, defaultValue);
    }

    public static Boolean getBooleanParam(Map<String, String> params, String key, Boolean defaultValue) {
        return getBooleanParam(params, key, defaultValue, "1");
    }

    public static Boolean getBooleanParam(Map<String, String> params, String key, Boolean defaultValue, String trueValue) {
        if (params.containsKey(key))
            return trueValue.equals(params.get(key));
        else
            return defaultValue;
    }

    public static Boolean getBooleanParamNullable(Map<String, String> params, String key, String trueValue) {
        if (params.containsKey(key))
            return trueValue.equals(params.get(key));
        else
            return null;
    }

    public static Integer getIntegerParam(Map<String, String> params, String key, Integer defaultValue) {
        try {
            return Integer.parseInt(params.getOrDefault(key, defaultValue.toString()));
        } catch (Exception ignored) {
            return defaultValue;
        }
    }

    public static Integer getPercentParam(Map<String, String> params, String key, Integer defaultValue) {
        Integer res;
        try {
            res = Integer.parseInt(params.getOrDefault(key, defaultValue.toString()));
        } catch (Exception ignored) {
            res = defaultValue;
        }

        if (res > 100)
            res = 100;
        else if (res < 0)
            res = 0;
        return res;
    }

    public static Double getDoubleParam(Map<String, String> params, String key, Double defaultValue) {
        try {
            return Double.parseDouble(params.getOrDefault(key, defaultValue.toString()));
        } catch (Exception ignored) {
            return defaultValue;
        }
    }

    public static Boolean isTimerTask(Map<String, String> params) {
        return !getStringParam(params, PARAM_AT_TIME, "").isEmpty() ||
                !getStringParam(params, PARAM_IN_TIME, "").isEmpty();
    }
}
