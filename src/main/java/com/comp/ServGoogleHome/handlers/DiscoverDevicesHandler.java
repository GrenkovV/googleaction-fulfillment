package com.comp.SereviceGoogleHome.handlers;

import com.comp.SereviceGoogleHome.controllers.LoginController;
import com.comp.SereviceGoogleHome.models.DeviceInfo;
import com.comp.SereviceGoogleHome.models.Fulfillment;
import com.comp.SereviceGoogleHome.services.SereviceCloudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DiscoverDevicesHandler extends SmartHomeHandler {

    private final Logger logger = LoggerFactory.getLogger(DiscoverDevicesHandler.class);

    @Override
    public Fulfillment process(String accessToken, Map<String, String> params, SereviceCloudService SereviceCloudService, String userId) {
        List<DeviceInfo> deviceList = SereviceCloudService.getDeviceList(userId, true);
        deviceList.forEach(deviceInfo -> {
            logger.info("Device[" + deviceInfo.getDevId() + "]: " + deviceInfo.getName() + " | " + deviceInfo.getCustomName());
        });

        if (deviceList.isEmpty()) {
            return new Fulfillment("I can't find any device in your account. What should I do next?");
        } else {
            String message = "I found " + deviceList.size() + " devices. You can refer to ";
            message += deviceList.stream().map(DeviceInfo::getCustomName).collect(Collectors.joining(", ")) + ".";
            message += " Waiting for your instructions";
            return new Fulfillment(message);
        }
    }
}
