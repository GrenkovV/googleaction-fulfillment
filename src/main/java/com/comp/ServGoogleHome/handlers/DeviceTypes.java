package com.comp.SereviceGoogleHome.handlers;

public enum DeviceTypes {
    UNKNOWN,
    LIGHT_BULB,
    OUTLET,
    POWER_STRIP
}
