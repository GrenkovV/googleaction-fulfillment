package com.comp.SereviceGoogleHome.services;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.UpdateTimeToLiveRequest;
import com.comp.SereviceGoogleHome.models.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AwsDynamoDBService {

    private static Logger logger = LoggerFactory.getLogger(AwsDynamoDBService.class);

    private static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
            .withRegion(Regions.US_EAST_1)
            //.withCredentials((AWSCredentialsProvider) new BasicAWSCredentials("", ""))
            .build();

    private static DynamoDBMapper mapper = new DynamoDBMapper(client);

    public UserInfo getUserInfo(String token) {

        UserInfo keySchema = new UserInfo().setAccessToken(token);

        try {
            UserInfo result = mapper.load(keySchema);
            if (result == null) {
                logger.info("No matching song was found");
            }
            return result;
        } catch (Exception e) {
            logger.warn("Unable to retrieve data: ");
            logger.warn(e.getMessage());
        }

        return null;
    }

    public boolean saveUserInfo(UserInfo userInfo) {

        try {
            mapper.save(userInfo);
            return true;
        } catch (Exception e) {
            logger.warn("Unable to save data: ");
            logger.warn(e.getMessage());
        }

        return false;
    }

    public boolean removeUserInfo(String token) {

        UserInfo keySchema = new UserInfo().setAccessToken(token);

        try {
            mapper.delete(keySchema);
            return true;
        } catch (Exception e) {
            logger.warn("Unable to delete data: ");
            logger.warn(e.getMessage());
        }

        return false;
    }

}
