package com.comp.SereviceGoogleHome.services;


import com.comp.SereviceGoogleHome.models.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LoginService {

    private final Logger logger = LoggerFactory.getLogger(LoginService.class);

    private final SereviceCloudService SereviceCloudService;

    @Autowired
    public LoginService(SereviceCloudService SereviceCloudService) {
        this.SereviceCloudService = SereviceCloudService;
    }

    private static Map<String, UserInfo> usersCache = new HashMap<>();
    private long usersCacheUpdateTime = 0;

    private void refreshUsersCache() {
        if (System.currentTimeMillis() - 1000 * 60 * 5 > usersCacheUpdateTime) {

            usersCache = new HashMap<>();
            int count = 100;
            int offset = 0;
            while (SereviceCloudService.getUsersList(usersCache, offset, count)) {
                offset += count;
            }

            usersCacheUpdateTime = System.currentTimeMillis();
        }
    }


    public UserInfo validateUser(String username, String password) {
        refreshUsersCache();

        UserInfo userInfo = usersCache.get(username);
        if (userInfo == null)
            return null;

        if (userInfo.getUsername().equals(username) && SereviceCloudService.verifyPassword(username, password))
            return userInfo;
        else
            return null;
    }
}