package com.comp.SereviceGoogleHome.controllers;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.comp.SereviceGoogleHome.handlers.*;
import com.comp.SereviceGoogleHome.models.DeviceInfo;
import com.comp.SereviceGoogleHome.models.Fulfillment;
import com.comp.SereviceGoogleHome.models.RestResponse;
import com.comp.SereviceGoogleHome.models.UserInfo;
import com.comp.SereviceGoogleHome.services.AwsDynamoDBService;
import com.comp.SereviceGoogleHome.services.SereviceCloudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class SmartHomeController {

    private final Logger logger = LoggerFactory.getLogger(SmartHomeController.class);

    private final SereviceCloudService SereviceCloudService;
    private final AwsDynamoDBService awsDynamoDBService;

    @Autowired
    public SmartHomeController(SereviceCloudService SereviceCloudService, AwsDynamoDBService awsDynamoDBService) {
        this.SereviceCloudService = SereviceCloudService;
        this.awsDynamoDBService = awsDynamoDBService;
    }

    private static final Map<String, SmartHomeHandler> handlers = new HashMap<>();

    static {
        handlers.put("intent.welcome", new WelcomeHandler());
        handlers.put("intent.discover", new DiscoverDevicesHandler());
        handlers.put("intent.power.action", new PowerActionHandler());
        handlers.put("intent.power.status", new PowerStatusHandler());
        handlers.put("intent.lightbulb.brightness", new LightBrightnessHandler());
        handlers.put("intent.lightbulb.color", new LightColorHandler());
        handlers.put("intent.lightbulb.white", new LightWhiteHandler());
        handlers.put("intent.lightbulb.mood", new LightMoodHandler());
        handlers.put("intent.homeaway", new HomeAwayHandler());
    }

    @RequestMapping(value = "/fulfillment", method = RequestMethod.POST)
    public @ResponseBody
    Fulfillment fulfillmentHandler(HttpServletRequest req,
                                   HttpServletResponse resp) throws IOException {
        String jsonString;
        try {
            jsonString = req.getReader().lines().collect(Collectors.joining());
        } catch (Exception ex) {
            logger.warn("Failed to get Json fulfillment param for Dialogflow webhook");
            return new Fulfillment("Something went wrong");
        }

        JSONObject jsonObj = (JSONObject) JSON.parse(jsonString);
        String accessToken;
        try {
            accessToken = jsonObj.getJSONObject("originalRequest").getJSONObject("data")
                    .getJSONObject("user").getString("accessToken");
        } catch (Exception ex) {
            logger.warn("Failed to get access token from request");
            return new Fulfillment("Sorry, but I don't recognize you. Something is wrong with authorization. What should I do next?");
        }

        UserInfo user = LoginController.getUserId(accessToken);
        if (user == null) {
            user = awsDynamoDBService.getUserInfo(accessToken);
        }

        if (user == null) {
            logger.warn("Can't find userId by specified accessToken");
            return new Fulfillment("Sorry, but I don't recognize you. Something is wrong with authorization. What should I do next?");
        }

        String intent;
        try {
            intent = jsonObj.getJSONObject("result").getString("action");
        } catch (Exception ex) {
            logger.warn("Failed to get intent request");
            return new Fulfillment("Something went wrong");
        }

        SmartHomeHandler handler = handlers.get(intent);
        if (handler == null) {
            return new Fulfillment("Sorry but I don't understand your request");
        }

        // Get params from Original Request
        Map<String, String> params = new HashMap<>();
        JSONObject jsonParams = jsonObj.getJSONObject("result").getJSONObject("parameters");
        jsonParams.keySet().forEach(key -> {
            params.put(key, jsonParams.getString(key));
        });

        try {
            return handler.process(accessToken, params, SereviceCloudService, user.getId());
        } catch (RuntimeException ex) {
            logger.warn("Failed to process intent", ex);
            return new Fulfillment("Sorry, but something went wrong. What should I do next?");
        }

        //return new Fulfillment("TEST");
    }

    @RequestMapping(value = "/getTask", method = RequestMethod.GET)
    public @ResponseBody
    RestResponse getTimerTaskTEST(HttpServletRequest req,
                                  HttpServletResponse resp) {

        SereviceCloudService.getTimerTasks("002004695ccf7f3a5c51");
        return RestResponse.asSuccess("Done");
    }

    @RequestMapping(value = "/getDevice", method = RequestMethod.GET)
    public @ResponseBody
    RestResponse getDeviceInfoTEST(HttpServletRequest req,
                                   HttpServletResponse resp) {

        DeviceInfo deviceInfo = SereviceCloudService.getDeviceInfo("002003115ccf7ff738b7");
        //DeviceInfo deviceInfo = SereviceCloudService.getDeviceInfo("002004695ccf7f3a5c51");
        return RestResponse.asSuccess("Done", deviceInfo.getDps());
    }

    @RequestMapping(value = "/getDeviceList", method = RequestMethod.GET)
    public @ResponseBody
    RestResponse getDeviceInfoListTEST(HttpServletRequest req,
                                       HttpServletResponse resp) {

        List<DeviceInfo> deviceList = SereviceCloudService.getDeviceList("az14891467944426DBgK", false);
        return RestResponse.asSuccess("Done", deviceList);
    }
}
