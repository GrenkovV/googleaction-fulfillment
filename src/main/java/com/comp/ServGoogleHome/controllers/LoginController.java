package com.comp.SereviceGoogleHome.controllers;

import com.comp.SereviceGoogleHome.models.AccessTokenResponse;
import com.comp.SereviceGoogleHome.models.UserInfo;
import com.comp.SereviceGoogleHome.services.AwsDynamoDBService;
import com.comp.SereviceGoogleHome.services.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
public class LoginController {

    private final Logger logger = LoggerFactory.getLogger(LoginController.class);

    private final LoginService loginService;
    private final AwsDynamoDBService awsDynamoDBService;

    private static Map<String, UserInfo> usersAuthMap = new HashMap<>();

    public static UserInfo getUserId(String code) {
        return usersAuthMap.get(code);
    }

    @Autowired
    public LoginController(LoginService loginService, AwsDynamoDBService awsDynamoDBService) {
        this.loginService = loginService;
        this.awsDynamoDBService = awsDynamoDBService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginPage(ModelMap model,
                                @RequestParam(value = "from", required = true, defaultValue = "") String from,
                                @RequestParam(value = "state", required = true, defaultValue = "") String state,
                                @RequestParam(value = "scope", required = true, defaultValue = "") String scope,
                                @RequestParam(value = "response_type", required = true, defaultValue = "") String response_type,
                                @RequestParam(value = "client_id", required = true, defaultValue = "") String client_id,
                                @RequestParam(value = "redirect_uri", required = true, defaultValue = "") String redirectUri) {

        model.put("redirectUri", redirectUri);
        model.put("from", from);
        model.put("state", state);
        model.put("scope", scope);
        model.put("response_type", response_type);

        return "loginPage";
    }

    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public ResponseEntity doLogin(Map<String, Object> model,
                                  HttpServletResponse resp) throws IOException {

        //resp.sendError(403, "Unknown response_type");
        return ResponseEntity.ok("pong");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String doLogin(Map<String, Object> model, @RequestParam String user,
                          @RequestParam String pass,
                          @RequestParam String redirectUri,
                          @RequestParam String from,
                          @RequestParam String state,
                          @RequestParam String scope,
                          @RequestParam String response_type,
                          HttpServletResponse resp) throws IOException {

        UserInfo validUser = loginService.validateUser(user, pass);
        if (validUser == null) {
            logger.warn("VALIDATION: FAIL");
            model.put("errorMessage", "Invalid Credentials");
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
                    "Invalid Credentials");
            return null;
        }
        model.put("name", user);
        model.put("pass", pass);
        model.put("scope", scope);
        model.put("response_type", response_type);

        String code = UUID.randomUUID().toString().replaceAll("-", "");

        usersAuthMap.put(code, validUser);

        String redir = "";
        if (response_type.equals("code")) {
            usersAuthMap.put(code, validUser);
            redir = redirectUri + "?state=" + state +
                    "&code=" + code +
                    "&base_redirect=" + URLEncoder.encode("https://layla.amazon.com", "utf-8");

            resp.sendRedirect(redir);
            //resp.setHeader("Location", redirectUri);
            return null;
        } else if (response_type.equals("token")) {
            AccessTokenResponse accessTokenResponse = new AccessTokenResponse();
            validUser.setAccessToken(accessTokenResponse.getAccess_token());
            validUser.setRefreshToken(accessTokenResponse.getRefresh_token());
            usersAuthMap.put(accessTokenResponse.getAccess_token(), validUser);
            awsDynamoDBService.saveUserInfo(validUser);

            redir = redirectUri + "#state=" + state +
                    "&access_token=" + accessTokenResponse.getAccess_token() +
                    "&token_type=bearer";

            resp.sendRedirect(redir);
            //resp.setHeader("Location", redirectUri);
            return null;
        }

        resp.sendError(403, "Unknown response_type");
        return null;
    }

    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public ResponseEntity<AccessTokenResponse> getAccessTokenP(Map<String, Object> model,
                                                               @RequestParam String client_id,
                                                               @RequestParam String client_secret,
                                                               @RequestParam(required = false) String code,
                                                               @RequestParam String grant_type,
                                                               @RequestParam(required = false) String redirect_uri,
                                                               @RequestParam(required = false) String refresh_token,
                                                               HttpServletResponse resp) throws IOException {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json");

        AccessTokenResponse accessTokenResponse = new AccessTokenResponse();

        if (code != null) {
            UserInfo user = usersAuthMap.get(code);
            if (user != null) {
                user.setAccessToken(accessTokenResponse.getAccess_token())
                        .setRefreshToken(accessTokenResponse.getRefresh_token());

                usersAuthMap.remove(code);
                usersAuthMap.put(accessTokenResponse.getAccess_token(), user);
                awsDynamoDBService.saveUserInfo(user);
                user.makeComplementary();
                usersAuthMap.put(accessTokenResponse.getRefresh_token(), user);
                awsDynamoDBService.saveUserInfo(user);
                return new ResponseEntity<>(accessTokenResponse, responseHeaders, HttpStatus.OK);
            } else {
                accessTokenResponse.clear();
                return new ResponseEntity<>(null, responseHeaders, HttpStatus.BAD_REQUEST);
            }
        } else if (refresh_token != null) {
            UserInfo user = usersAuthMap.get(refresh_token);
            if (user == null)
                user = awsDynamoDBService.getUserInfo(refresh_token);

            if (user != null) {
                usersAuthMap.remove(user.getAccessToken());
                usersAuthMap.remove(user.getRefreshToken());

                user.setAccessToken(accessTokenResponse.getAccess_token())
                        .setRefreshToken(accessTokenResponse.getRefresh_token());

                usersAuthMap.put(accessTokenResponse.getAccess_token(), user);
                awsDynamoDBService.saveUserInfo(user);
                user.makeComplementary();
                usersAuthMap.put(accessTokenResponse.getRefresh_token(), user);
                awsDynamoDBService.saveUserInfo(user);

                return new ResponseEntity<>(accessTokenResponse, responseHeaders, HttpStatus.OK);
            } else {
                accessTokenResponse.clear();
                return new ResponseEntity<>(null, responseHeaders, HttpStatus.BAD_REQUEST);
            }
        }

        return new ResponseEntity<>(accessTokenResponse, responseHeaders, HttpStatus.BAD_REQUEST);
    }
}
