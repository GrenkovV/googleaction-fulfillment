package com.comp.SereviceGoogleHome.commands;

import com.comp.SereviceGoogleHome.handlers.DeviceTypes;
import com.comp.SereviceGoogleHome.models.DeviceInfo;

public class CommandBuilderFactory {

    public static CommandBuilder getCommandBuilder(DeviceInfo device) {
        if (device.detectType() == DeviceTypes.POWER_STRIP) {
            return new PowerstripCommands();
        } else if (device.detectType() == DeviceTypes.OUTLET) {
            return new OutletCommands();
        } else if (device.detectType() == DeviceTypes.LIGHT_BULB) {
            return new LampCommands();
        } else {
            return new OnOffCommands();
        }
    }

}
