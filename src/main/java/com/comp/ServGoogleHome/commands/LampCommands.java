package com.comp.SereviceGoogleHome.commands;

import com.comp.SereviceGoogleHome.handlers.ParamsConst;
import com.comp.SereviceGoogleHome.models.CommandResponse;
import com.comp.SereviceGoogleHome.models.DeviceInfo;
import com.comp.SereviceGoogleHome.services.SereviceCloudService;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class LampCommands extends CommandBuilder {
    private static final String POWER_DPID = "1";
    private static final String WORK_MODE_DPID = "2";
    private static final String BRIGHT_DPID = "3";
    private static final String TEMP_DPID = "4";
    private static final String COLOR_DPID = "5";

    private static Map<String, String> moodMap = new HashMap<>();

    static {
        moodMap.put("RELAXING", "0xffad1b");
        moodMap.put("READING", "0xffbe53");
        moodMap.put("FOCUS", "0xFFF49E");
        moodMap.put("EXERCISE", "0xb9d6ff");
        moodMap.put("MAX_BRIGHT", "0xffffff");
        moodMap.put("MEDIA", "0x14b0d8");
        moodMap.put("NIGHT_LIGHT", "0x402400");
        moodMap.put("ROMANTIC", "0xc761b9");
        moodMap.put("CANDLE_LIGHT", "0xf8dd61");
        moodMap.put("DAYLIGHT", "0xc777bc");
    }


    private static Logger logger = LoggerFactory.getLogger(LampCommands.class);

    private static Color getColorByName(String colorName) {
        try {
            return (Color) Color.class.getField(colorName.replace(" ", "_").toUpperCase()).get(null);
        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
            logger.warn("Can't find color [" + colorName + "]", e);
            return null;
        }
    }

    private static String makeColorSerevice(Color color, Integer colorBrightness) {
        String SereviceColor = String.format("%02X%02X%02X%04X%02X%02X",
                (int) (color.getRed() * 255 * colorBrightness / 100),
                (int) (color.getGreen() * 255 * colorBrightness / 100),
                (int) (color.getBlue() * 255 * colorBrightness / 100),
                (int) (color.getHue() * colorBrightness / 100),
                (int) (color.getSaturation() * 255 * colorBrightness / 100),
                (int) (color.getBrightness() * 255 * colorBrightness / 100));

        return SereviceColor;
    }

    @Override
    public Command buildCommand(DeviceInfo device, Map<String, String> params) {
        Boolean targetState = ParamsConst.getBooleanParamNullable(params, ParamsConst.PARAM_POWER_ACTION, "1");
        Boolean swHomeAway = ParamsConst.getBooleanParamNullable(params, ParamsConst.PARAM_HOME_AWAY, "1");
        int brightnessPercent = ParamsConst.getPercentParam(params, ParamsConst.PARAM_PERCENT, 100);
        String colorName = ParamsConst.getStringParam(params, ParamsConst.PARAM_COLOR_NAME, "");
        String lightMood = ParamsConst.getStringParam(params, ParamsConst.PARAM_LIGHT_MOOD, "");
        int whiteLightMode = ParamsConst.getIntegerParam(params, ParamsConst.PARAM_WHITE_LIGHT_MODE, 0);
        int brightenOrDim = ParamsConst.getIntegerParam(params, ParamsConst.PARAM_BRIGHTEN_DIM, 0);
        boolean swSetBrightness = ParamsConst.getBooleanParam(params, ParamsConst.PARAM_SET_BRIGHTNESS, false, "1");
        boolean swCheckStatus = ParamsConst.getBooleanParam(params, ParamsConst.PARAM_CHECK_STATUS, false, "1");

        if(swHomeAway != null)
            targetState = swHomeAway;

        Date date = getEqualizedTime(device.getTimeZoneStr(), params);

        if (swCheckStatus)
            return checkPowerStatus(device);

        if (!lightMood.isEmpty() && moodMap.containsKey(lightMood)) {
            if(lightMood.equals("MAX_BRIGHT")) {
                return setLightBulbWhiteMode(device, whiteLightMode, 100);
            } else {
                Color color = Color.web(moodMap.get(lightMood));
                return setLightBulbColor(device, color, brightnessPercent, date);
            }
        }

        if (!colorName.isEmpty()) {
            Color color = Color.web(colorName.replaceAll("\\s", ""));
            return setLightBulbColor(device, color, brightnessPercent, date);
        }

        if (whiteLightMode != 0) {
            return setLightBulbWhiteMode(device, whiteLightMode, brightnessPercent);
        }

        if (swSetBrightness) {
            if (brightenOrDim > 0 && brightnessPercent == 0 ||
                    ParamsConst.getStringParam(params, ParamsConst.PARAM_PERCENT, "").isEmpty()) {
                brightnessPercent = 30;
            }
            return setLightBulbBrightness(device, brightenOrDim, brightnessPercent);
        }

        return switchLightBulb(device, targetState, date);
    }

    private Command setLightBulbColor(DeviceInfo device, Color color, Integer colorBrightness, Date date) {
        return new Command(device) {
            @Override
            public CommandResponse execute(SereviceCloudService SereviceCloudService) {

                if (color == null)
                    return CommandResponse.failed("Sorry, but specified color not found. What should I do next?");

                Map<String, Object> dpsMap = new HashMap<>();

                String SereviceColor = makeColorSerevice(color, colorBrightness);
                //dpsMap.put(POWER_DPID, "true");
                dpsMap.put(WORK_MODE_DPID, "colour");
                dpsMap.put(COLOR_DPID, SereviceColor);

                device.syncDps(dpsMap);

                if (date == null)
                    return new CommandResponse(SereviceCloudService.publishDeviceDp(device.getDevId(), dpsMap));
                else
                    return new CommandResponse(SereviceCloudService.addTimerTask(device, dpsMap, date));
            }
        };
    }

    private Command setLightBulbWhiteMode(DeviceInfo device, int whiteMode, int brightness) {
        return new Command(device) {
            @Override
            public CommandResponse execute(SereviceCloudService SereviceCloudService) {

                Map<String, Object> dpsMap = new HashMap<>();

                //dpsMap.put(POWER_DPID, "true");
                dpsMap.put(WORK_MODE_DPID, "white");

                int temp = 0;
                int bright = 0;
                switch (whiteMode) {
                    case 1: // White
                        temp = 0;
                        bright = 0;
                        break;
                    case 2: // Warm
                        temp = 255;
                        bright = 255;
                        break;
                    case 3: // Cold
                        temp = 10;
                        bright = 255;
                        break;
                    case 4: // Daylight
                        temp = 50;
                        bright = 200;
                        break;
                    case 5: // Soft
                        temp = 100;
                        bright = 200;
                        break;
                }

                temp = temp * brightness / 100;
                bright = bright * brightness / 100;

                if (temp > 0)
                    dpsMap.put(TEMP_DPID, temp);
                if (bright > 0)
                    dpsMap.put(BRIGHT_DPID, bright);

                device.syncDps(dpsMap);

                return new CommandResponse(SereviceCloudService.publishDeviceDp(device.getDevId(), dpsMap));
            }
        };
    }

    private Command setLightBulbBrightness(DeviceInfo device, int direction, int brightness) {
        return new Command(device) {
            @Override
            public CommandResponse execute(SereviceCloudService SereviceCloudService) {

                // Refresh device data
                DeviceInfo deviceInfo = SereviceCloudService.getDeviceInfo(device.getDevId());

                //String stickToMode = deviceInfo.getDps().get("2").toString();

                //deviceInfo.getDps().put(POWER_DPID, "true");

                Integer lightBright = 255;
                try {
                    lightBright = Integer.parseInt(deviceInfo.getDps().get(BRIGHT_DPID).toString());
                } catch (Exception ex) {
                    logger.warn("Failed to get current Bright Level of " + device.getName() + " [" + device.getDevId() + "]", ex);
                    return new CommandResponse(false);
                }
                Integer lightTemp = Integer.parseInt(deviceInfo.getDps().get(TEMP_DPID).toString());
                Color lightColor = Color.web(deviceInfo.getDps().get(COLOR_DPID).toString().substring(0, 6));
                Integer currBrightPercent = 100 * lightBright / 255;
                //Double currColorPercent = Math.max(Math.max(lightColor.getRed(), lightColor.getGreen()), lightColor.getBlue());
                Double currColorPercent = lightColor.getBrightness();


                //int targetGreenPercent = (int) (lightColor.getGreen() * 100);
                //int targetBluePercent = (int) (lightColor.getBlue() * 100);

                Double targetColorPercent = 1.0 + ((double) brightness / 100.0 - currColorPercent) / currColorPercent;
                Integer targetBrightPercent = brightness;
                if (direction == 1) {
                    targetBrightPercent = currBrightPercent + brightness;
                    targetColorPercent = 1.0 + ((double) brightness / 100.0) / currColorPercent;
                } else if (direction == 2) {
                    targetBrightPercent = currBrightPercent - brightness;
                    targetColorPercent = 1.0 - ((double) brightness / 100.0) / currColorPercent;
                }
                /*if (targetColorPercent > 1.0)
                    targetColorPercent = 1.0;
                else if (targetColorPercent < 0.0)
                    targetColorPercent = 0.0;*/

                /*Double deltaColorPercent = targetColorPercent - currColorPercent;
                Color newColor = Color.color(Math.max(lightColor.getRed() + deltaColorPercent, 0.0),
                        Math.max(lightColor.getGreen() + deltaColorPercent, 0.0),
                        Math.max(lightColor.getBlue() + deltaColorPercent, 0.0));*/
                Color newColor = lightColor.deriveColor(1.0, 1.0, targetColorPercent, 1.0);

                if (targetBrightPercent > 100)
                    targetBrightPercent = 100;
                else if (targetBrightPercent < 10)
                    targetBrightPercent = 10;

                Map<String, Object> dpsMap = new HashMap<>();

                dpsMap.put(TEMP_DPID, 255 * targetBrightPercent / 100);
                dpsMap.put(BRIGHT_DPID, 255 * targetBrightPercent / 100);
                dpsMap.put(COLOR_DPID, makeColorSerevice(newColor, 100));

                device.syncDps(dpsMap);

                return new CommandResponse(SereviceCloudService.publishDeviceDp(device.getDevId(), dpsMap));
            }
        };
    }

    private Command checkPowerStatus(DeviceInfo device) {
        return new Command(device) {
            @Override
            public CommandResponse execute(SereviceCloudService SereviceCloudService) {
                // Refresh device data
                DeviceInfo targetDevice = SereviceCloudService.getDeviceInfo(device.getDevId());
                if (targetDevice.getDps().containsKey("1") &&
                        targetDevice.getDps().get("1").toString().equals("true")) {
                    return new CommandResponse(true, "Light bulb is currently on");
                } else {
                    return new CommandResponse(true, "Light bulb is currently off");
                }
            }
        };
    }

    public Command switchLightBulb(DeviceInfo device, Boolean value, Date date) {
        return new Command(device) {
            @Override
            public CommandResponse execute(SereviceCloudService SereviceCloudService) {
                if(value == null)
                    return new CommandResponse(false);

                Map<String, Object> dpsMap = new HashMap<>();
                dpsMap.put(POWER_DPID, value);
                device.syncDps(dpsMap);
                if (date == null)
                    return new CommandResponse(SereviceCloudService.publishDeviceDp(device.getDevId(), dpsMap));
                else
                    return new CommandResponse(SereviceCloudService.addTimerTask(device, dpsMap, date));
            }
        };
    }
}
