package com.comp.SereviceGoogleHome.commands;

import com.comp.SereviceGoogleHome.handlers.ParamsConst;
import com.comp.SereviceGoogleHome.models.CommandResponse;
import com.comp.SereviceGoogleHome.models.DeviceInfo;
import com.comp.SereviceGoogleHome.services.SereviceCloudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OnOffCommands extends CommandBuilder {
    private static final String POWER_DPID = "1";

    Logger logger = LoggerFactory.getLogger(OnOffCommands.class);

    @Override
    public Command buildCommand(DeviceInfo device, Map<String, String> params) {
        Boolean targetState = ParamsConst.getBooleanParamNullable(params, ParamsConst.PARAM_POWER_ACTION, "1");
        Boolean swHomeAway = ParamsConst.getBooleanParamNullable(params, ParamsConst.PARAM_HOME_AWAY, "1");
        boolean swCheckStatus = ParamsConst.getBooleanParam(params, ParamsConst.PARAM_CHECK_STATUS, false);

        if (swHomeAway != null)
            targetState = swHomeAway;

        if (swCheckStatus) {
            return checkPowerStatus(device);
        }

        Date date = getEqualizedTime(device.getTimeZoneStr(), params);

        return switchSocket(device, targetState, date);
    }

    private Command checkPowerStatus(DeviceInfo device) {
        return new Command(device) {
            @Override
            public CommandResponse execute(SereviceCloudService SereviceCloudService) {
                // Refresh device data
                DeviceInfo targetDevice = SereviceCloudService.getDeviceInfo(device.getDevId());
                if (targetDevice.getDps().get(POWER_DPID).toString().equals("true")) {
                    return new CommandResponse(true, "Device is currently on");
                } else {
                    return new CommandResponse(true, "Device is currently off");
                }
            }
        };
    }

    public Command switchSocket(DeviceInfo device, Boolean value, Date date) {
        return new Command(device) {
            @Override
            public CommandResponse execute(SereviceCloudService SereviceCloudService) {
                if (value == null)
                    return new CommandResponse(false);

                Map<String, Object> dpsMap = new HashMap<>();
                dpsMap.put(POWER_DPID, value);
                device.syncDps(dpsMap);
                if (date == null)
                    return new CommandResponse(SereviceCloudService.publishDeviceDp(device.getDevId(), dpsMap));
                else
                    return new CommandResponse(SereviceCloudService.addTimerTask(device, dpsMap, date));
            }
        };
    }
}
