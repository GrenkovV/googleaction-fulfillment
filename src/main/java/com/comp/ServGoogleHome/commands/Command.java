package com.comp.SereviceGoogleHome.commands;

import com.comp.SereviceGoogleHome.models.CommandResponse;
import com.comp.SereviceGoogleHome.models.DeviceInfo;
import com.comp.SereviceGoogleHome.services.SereviceCloudService;

public abstract class Command {

    private DeviceInfo device;

    public Command(DeviceInfo device) {
        this.device = device;
    }

    public DeviceInfo getDevice() {
        return device;
    }

    public void setDevice(DeviceInfo device) {
        this.device = device;
    }

    public abstract CommandResponse execute(SereviceCloudService SereviceCloudService);
}
