package com.comp.SereviceGoogleHome.commands;

import com.comp.SereviceGoogleHome.handlers.ParamsConst;
import com.comp.SereviceGoogleHome.models.DeviceInfo;

import java.sql.Time;
import java.util.*;

public abstract class CommandBuilder {

    Date getEqualizedTime(String deviceTZString, Map<String, String> params) {
        boolean in_at = true;
        String time = ParamsConst.getStringParam(params, ParamsConst.PARAM_IN_TIME, "");
        if (time.isEmpty()) {
            time = ParamsConst.getStringParam(params, ParamsConst.PARAM_AT_TIME, "");
            in_at = false;
        }

        if (time.isEmpty())
            return null;

        String timeParts[] = time.split(":");
        Calendar calendar = GregorianCalendar.getInstance();
        if (in_at) {
            calendar = Calendar.getInstance(TimeZone.getTimeZone("GTM"));
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeParts[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(timeParts[1]));
            calendar.set(Calendar.SECOND, Integer.parseInt(timeParts[2]));
            TimeZone deviceTimeZone = TimeZone.getTimeZone("GMT" + deviceTZString);
            if (deviceTimeZone == null)
                throw new RuntimeException("Time zone not found [" + deviceTimeZone + "]");

            int tzOffset = deviceTimeZone.getRawOffset() - TimeZone.getDefault().getRawOffset();

            calendar.add(Calendar.MILLISECOND, tzOffset);
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeParts[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(timeParts[1]));
            calendar.set(Calendar.SECOND, Integer.parseInt(timeParts[2]));
        }
        return calendar.getTime();
    }

    public abstract Command buildCommand(DeviceInfo device, Map<String, String> params);

}
