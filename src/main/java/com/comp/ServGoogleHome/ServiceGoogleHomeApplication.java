package com.comp.SereviceGoogleHome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SereviceGoogleHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SereviceGoogleHomeApplication.class, args);
	}
}
